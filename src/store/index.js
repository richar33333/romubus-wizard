import Vue from 'vue'
import Vuex from 'vuex'


Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    activeSubcategory: false,
    category: [],
    subCategory: []
  },
  mutations: {
    selectSubcategory(state, value) {
      state.activeSubcategory = value
    },

    getCategory(state, value) {
      state.category = value
    },

    getSubCategory(state, value) {
      state.subCategory = value
    }

  },
  actions: {
  },
  modules: {
  }
})
